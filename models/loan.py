from odoo import models, tools, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError

from odoo.exceptions import UserError


class ppfLoan(models.Model):
    _name = 'ppf.loan'

    employee_name = fields.Many2one('res.partner', string='Employee Name', required=True)
    subscription_date = fields.Date('Subscription Date', compute='compute_sub_date')
    retirement_date = fields.Date('Pension Date', compute='compute_retirement_date')
    loan_units_taken = fields.Float('Loan Units Taken', compute='_compute_loan_units_taken')
    loan_amount = fields.Float('Loan Amount', compute='_compute_loan_amount')
    loan_date = fields.Date('Loan Date', default=datetime.today())
    total_units_owned = fields.Float('Total Units Owned', compute='_compute_total_units_owned')
    value_of_units_owned = fields.Float('Units Owned Value', compute='_compute_value_units_owned')
    total_units_after_loan = fields.Float('Total Units After Loan', compute='_compute_total_units_after_loan')
    value_of_units_after_loan = fields.Float('Units After Loan Value', compute='_compute_value_units_after_loan')
    state = fields.Selection(
        [('pending', 'Pending'), ('o/s', 'O/S'), ('inv', 'Units Sold'), ('payable', 'Payable'), ('paid', 'Paid'),
         ('rejected', 'Rejected')],
        required=True, default='pending', compute='_compute_state')
    unit_balance_ids = fields.One2many('ppf.year.unit.balance', 'loan_id')
    loans_taken_ids = fields.One2many('ppf.loans.taken.before', 'loan_id')
    loans_entry_items_ids = fields.One2many('ppf.loan.entry.items', 'loan_id')
    fund_price_ids = fields.One2many('ppf.fund.price', 'loan_id')
    fund = fields.Many2one('ppf.fund', string='Fund', default=lambda self: self._default_fund())
    loan_allocated = fields.Boolean('is Units Allocated ?', default=False)
    total_emp_units = fields.Float('Total Own ', compute='compute_total')
    total_emp_booster_units = fields.Float('Total Emp Booster', compute='compute_total')
    total_comp_booster_units = fields.Float('Total Comp Booster', compute='compute_total')
    total_company_units = fields.Float('Total Company', compute='compute_total')
    invoice_ids = fields.One2many('account.invoice', 'loan_id', string='Invoices', readonly=True)
    validate = fields.Boolean('validate ?', default=False)
    invoice_created = fields.Boolean('invoice created ?', default=False)
    # units_sold = fields.Boolean('invoice created ?', default=False)
    bill_created = fields.Boolean('bill created ?', default=False)

    @api.one
    @api.depends('invoice_ids')
    def _compute_state(self):
        # self.units_allocated = False
        inv_id = self.env['account.invoice'].search([('loan_id', '=', self.id), ('type', '=', 'in_invoice')])
        unit_id = self.env['ppf.unit'].search([('loan_id', '=', self.id)])
        sub_id = self.env['ppf.subscription.line'].search([('loan_id', '=', self.id)])
        self.state = 'pending'
        if self.validate == True:
            self.state = 'o/s'
        for record in self.invoice_ids:
            if record.loan_id.id == self.id:
                if record.state == 'paid':
                    self.state = 'inv'
                    if unit_id:
                        pass
                    else:
                        for rec in self.loans_entry_items_ids:
                            own_units = -rec.emp_units_taken
                            company_units = -rec.comp_units_taken
                            emp_booster_units = -rec.emp_booster_units_taken
                            comp_booster_units = -rec.comp_booster_units_taken
                            own = -rec.own
                            company = -rec.company
                            emp_booster = -rec.emp_booster
                            comp_booster = -rec.comp_booster
                            self.env['ppf.unit'].create({
                                'name': self.employee_name.id,
                                'member_id': self.employee_name.member_id,
                                'product': rec.fund_name.id,
                                'own_units': own_units,
                                'company_units': company_units,
                                'emp_booster_units': emp_booster_units,
                                'comp_booster_units': comp_booster_units,
                                'own': own,
                                'company': company,
                                'emp_booster': emp_booster,
                                'comp_booster': comp_booster,
                                # 'today_own_units_value': own,
                                # 'today_company_units_value': company,
                                # 'today_emp_booster_units_value': emp_booster,
                                # 'today_comp_booster_units_value': comp_booster,
                                'date': self.loan_date,
                                'department': self.employee_name.department.id,
                                'state': 'loan',
                                'loan_id': self.id,
                            })
            if self.bill_created == True:
                self.state = 'payable'
            if record.loan_id.id == self.id and record.type == 'in_invoice':
                if record.state == 'paid':
                    self.state = 'paid'
                    if sub_id:
                        pass
                    else:
                        for x in self.loans_entry_items_ids:
                            own = -x.own
                            company = -x.company
                            emp_booster = -x.emp_booster
                            comp_booster = -x.comp_booster
                            self.env['ppf.subscription.line'].create({
                                'member_name': self.employee_name.id,
                                'member_id': self.employee_name.member_id,
                                # 'product': rec.fund_name.id,
                                'own': own,
                                'company': company,
                                'emp_booster': emp_booster,
                                'comp_booster': comp_booster,
                                # 'date': datetime.today(),
                                'department': self.employee_name.department.id,
                                'states': 'loan',
                                'loan_id': self.id,
                            })

    @api.multi
    def create_customer_bill(self):
        inv_id = self.env['account.invoice'].search([('loan_id', '=', self.id), ('type', '=', 'in_invoice')])
        if self.state == 'inv':
            if inv_id:
                pass
            else:
                data = []
                for rec in self.loans_entry_items_ids:
                    z = (0, 0, {
                        'name': 'Invoice For Loan',
                        'quantity': 1,
                        'product_id': rec.fund_name.id,
                        'price_unit': rec.value_of_units_owned,
                        'account_id': self.employee_name.property_account_payable_id.id,
                    })
                    data.append(z)
                self.env['account.invoice'].create({
                    'type': 'in_invoice',
                    'partner_id': self.employee_name.id,
                    'user_id': self.env.user.id,
                    'loan_id': self.id,
                    'origin': 'Loan',
                    # 'state': 'open',
                    'date_invoice': self.loan_date,
                    'date_due': self.loan_date,
                    'invoice_line_ids': data,
                })
                self.bill_created = True

    @api.one
    @api.depends('total_units_owned')
    def compute_total(self):
        units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name)])
        for rec in units:
            self.total_emp_booster_units += rec.emp_booster_units
            self.total_comp_booster_units += rec.comp_booster_units
            self.total_emp_units += rec.own_units
            self.total_company_units += rec.company_units

    @api.model
    def _default_fund(self):
        return self.env['ppf.fund'].search([], limit=1)

    @api.depends('employee_name')
    @api.multi
    def compute_sub_date(self):
        self.subscription_date = self.employee_name.subscription_date

    @api.depends('employee_name')
    @api.multi
    def compute_retirement_date(self):
        self.retirement_date = self.employee_name.pension_date

    @api.depends('employee_name')
    @api.one
    def _compute_total_units_owned(self):
        units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name)])
        for rec in units:
            self.total_units_owned += rec.total_units

    @api.depends('employee_name', 'loans_entry_items_ids')
    @api.one
    def _compute_value_units_owned(self):
        if self.loan_date:
            loan_date = datetime.strptime(self.loan_date, '%Y-%m-%d').date()
            last_month = loan_date - relativedelta(months=1)
            last_date = str(last_month)
            for rec in self.loans_entry_items_ids:
                for x in rec.fund_name.product_pricing:
                    if x.date_from <= last_date <= x.date_to:
                        self.value_of_units_owned += rec.value_of_units_owned
                        print(x.price)

    @api.depends('loans_entry_items_ids')
    @api.one
    def _compute_loan_units_taken(self):
        for rec in self.loans_entry_items_ids:
            self.loan_units_taken += rec.emp_units_taken + rec.comp_units_taken + rec.emp_booster_units_taken + rec.comp_booster_units_taken

    @api.depends('loans_entry_items_ids')
    @api.one
    def _compute_loan_amount(self):
        for rec in self.loans_entry_items_ids:
            self.loan_amount += (
                                        rec.emp_units_taken + rec.comp_units_taken + rec.emp_booster_units_taken + rec.comp_booster_units_taken) * rec.amount

    @api.one
    @api.depends('total_units_owned', 'loan_units_taken')
    def _compute_total_units_after_loan(self):
        self.total_units_after_loan = self.total_units_owned - self.loan_units_taken

    @api.depends('loans_entry_items_ids')
    @api.one
    def _compute_value_units_after_loan(self):
        self.value_of_units_after_loan = self.value_of_units_owned

    @api.depends('loans_entry_items_ids')
    @api.multi
    def approve(self):
        date = datetime.today()
        print(date)
        loan_surr_rules = self.env["loan.rules"].search([('rule_id', '=', 1)])
        months_to_allowed = relativedelta(months=loan_surr_rules.first_loan_rule)
        months_between_loans = relativedelta(months=loan_surr_rules.second_loan_rule)
        validate_date_to_allowed = datetime.strptime(self.subscription_date, '%Y-%m-%d') + months_to_allowed

        if self.loans_taken_ids:
            validate_date_between_loans = datetime.strptime(self.loans_taken_ids.date,
                                                            '%Y-%m-%d') + months_between_loans
            if validate_date_between_loans > date:
                raise ValidationError('loan is only allowed 30 month from last loan date')

        elif validate_date_to_allowed > date:
            raise ValidationError('loans s only allowed after 60 month from subscription date')
        else:
            data = []
            for rec in self.loans_entry_items_ids:
                z = (0, 0, {
                    'name': 'Invoice For Loan112',
                    'quantity': 1,
                    'product_id': rec.fund_name.id,
                    'price_unit': rec.value_of_units_owned,
                    'account_id': rec.fund_name.property_account_income_id.id,
                })
                data.append(z)
            self.env['account.invoice'].create({
                'type': 'out_invoice',
                'partner_id': self.employee_name.department.id,
                'user_id': self.env.user.id,
                'loan_id': self.id,
                'origin': 'Loan',
                'date_invoice': self.loan_date,
                'date_due': self.loan_date,
                'invoice_line_ids': data,
            })
            self.invoice_created = True
            # bill.action_invoice_open()
            # self.state = 'o/s'

    def reject(self):
        self.state = 'rejected'

    # @api.model_cr
    @api.one
    def compute_total_emp_units(self):
        funds = self.env['product.template'].search([])
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name), ('product', '=', fund.id)])
            seen = []
            fund_seen = []
            own = 0.0
            company = 0.0
            emp_booster = 0.0
            comp_booster = 0.0
            for rec in units:
                x = datetime.strptime(rec.date, '%Y-%m-%d').year
                print(x)
                if rec.product in seen:
                    print('duplicated')
                else:
                    own += rec.own_units
                    company += rec.company_units
                    emp_booster += rec.emp_booster_units
                    comp_booster += rec.comp_booster_units
                    self.env.cr.execute("INSERT INTO  ppf_year_unit_balance (year, product, total_emp_units,"
                                        "total_company_units,total_emp_booster_units,total_comp_booster_units, "
                                        "loan_id) VALUES (%s,%s,%s,%s,%s,%s,%s) "
                                        , (datetime.strptime(rec.date, '%Y-%m-%d').year, rec.product.id, own
                                           , company, emp_booster, comp_booster, self.id))
                    seen.append(rec.product)
            self.validate = True

        # emp_units_owned = 0.0
        # comp_units_owned = 0.0
        # booster_units_owned = 0.0
        # units = self.env['ppf.unit.yearly.balance'].search([('name', '=', self.employee_name.name)])
        # for rec in units:
        #     emp_units_owned += rec.total_emp_units
        #     comp_units_owned += rec.total_comp_units
        #     booster_units_owned += rec.total_booster_units
        # emp_units_taken = 0.0
        # comp_units_taken = 0.0
        # booster_units_taken = 0.0
        # for record in self.loans_entry_items_ids:
        #     emp_units_taken += record.emp_units_taken
        #     comp_units_taken += record.comp_units_taken
        #     booster_units_taken += record.booster_units_taken
        # emp_units_after_loan = emp_units_owned - emp_units_taken
        # comp_units_after_loan = comp_units_owned - comp_units_taken
        # booster_units_after_loan = booster_units_owned - booster_units_taken
        loans = self.env['ppf.loan'].search([('employee_name', '=', self.employee_name.name),
                                             ('state', '=', 'paid')])
        for record in loans:
            for rec in record.loans_entry_items_ids:
                self.env.cr.execute("INSERT INTO  ppf_loans_taken_before (date, fund_name, emp_units_taken,"
                                    "comp_units_taken, emp_booster_units_taken,comp_booster_units_taken, loan_id) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                                    , (rec.date, rec.fund_name.id, rec.emp_units_taken, rec.comp_units_taken,
                                       rec.emp_booster_units_taken, rec.comp_booster_units_taken, self.id))

        funds = self.env['product.template'].search([])
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name),
                                                 ('product', '=', fund.id)])
            for unit in units:
                if unit:
                    if unit.product in fund_seen:
                        print('duplicated')
                    else:
                        self.env.cr.execute("INSERT INTO  ppf_fund_price (fund_name, loan_id) VALUES (%s,%s)"
                                            , (unit.product.id, self.id))
                        fund_seen.append(unit.product)
        self.loan_allocated = True
        # self.validate = True


class ppfYearUnitBalance(models.Model):
    _name = 'ppf.year.unit.balance'

    year = fields.Integer('Year')
    product = fields.Many2one('product.template', string='Product')
    total_emp_units = fields.Float('Total Emp.Units')
    total_company_units = fields.Float('Total Comp.Units')
    total_emp_booster_units = fields.Float('Total Emp  Booster Units')
    total_comp_booster_units = fields.Float('Total Comp Booster Units')
    total = fields.Float('Total', compute='compute_total')
    loan_id = fields.Many2one('ppf.loan', string='Loan', ondelete='cascade')

    @api.one
    @api.depends('total_emp_units', 'total_emp_booster_units', 'total_comp_booster_units', 'total_company_units')
    def compute_total(self):
        self.total = self.total_company_units + self.total_emp_units + self.total_emp_booster_units + self.total_comp_booster_units


class ppfLoansTakenBefore(models.Model):
    _name = 'ppf.loans.taken.before'

    date = fields.Date('Date')
    fund_name = fields.Many2one('product.template', string='Product')
    emp_units_taken = fields.Float('Emp Units Taken')
    emp_amount = fields.Float('Emp Amount', compute='compute_amount')
    comp_units_taken = fields.Float('Comp.Units Taken')
    comp_amount = fields.Float('Comp Amount', compute='compute_amount')
    emp_booster_units_taken = fields.Float('Total Emp Booster.Units Taken')
    comp_booster_units_taken = fields.Float('Total Comp Booster.Units Taken')
    booster_amount = fields.Float('Booster Amount', compute='compute_amount')
    units_taken = fields.Float('Total Units Taken', compute='compute_amount')
    amount = fields.Float('Total Amount', compute='compute_amount')
    loan_id = fields.Many2one('ppf.loan', string='Loan', ondelete='cascade')

    @api.depends('fund_name')
    @api.one
    def compute_amount(self):
        if self.fund_name:
            for rec in self.fund_name.product_pricing:
                if rec.date_from <= self.date <= rec.date_to:
                    self.emp_amount = self.emp_units_taken * rec.price
                    self.comp_amount = self.comp_units_taken * rec.price
                    self.booster_amount = (
                                                  self.emp_booster_units_taken + self.comp_booster_units_taken) * rec.price
                    self.units_taken = self.emp_units_taken + self.comp_units_taken + self.emp_booster_units_taken + self.comp_booster_units_taken
                    self.amount = self.units_taken * rec.price


class ppfLoanEntryItems(models.Model):
    _name = 'ppf.loan.entry.items'

    date = fields.Date('Date')
    fund_name = fields.Many2one('product.template', string='Product', required=True)
    emp_units_taken = fields.Float('Emp Units Taken')
    comp_units_taken = fields.Float('Comp Units Taken')
    emp_booster_units_taken = fields.Float('Booster Units Taken')
    comp_booster_units_taken = fields.Float('Booster Units Taken')
    amount = fields.Float('Unit Price', compute='compute_amount', store=True)
    loan_id = fields.Many2one('ppf.loan', string='Loan')
    this_year_units_ids = fields.Many2one('ppf.year.unit.balance', string='Loan', ondelete='cascade')
    own = fields.Float('Emp Units Value', compute='compute_values')
    company = fields.Float('Comp Units Value', compute='compute_values')
    emp_booster = fields.Float(' Emp Booster Value', compute='compute_values')
    comp_booster = fields.Float('Comp Booster Value', compute='compute_values')
    value_of_units_owned = fields.Float('Own Units Value', compute='compute_values')

    @api.depends('loan_id')
    @api.one
    def compute_amount(self):
        if self.fund_name:
            loan_date = datetime.strptime(self.date, '%Y-%m-%d').date()
            last_month = loan_date - relativedelta(months=1)
            last_date = str(last_month)
            for rec in self.fund_name.product_pricing:
                if rec.date_from <= last_date <= rec.date_to:
                    self.amount = rec.price

    @api.one
    @api.depends('emp_units_taken', 'comp_units_taken', 'comp_booster_units_taken', 'emp_booster_units_taken')
    def compute_values(self):
        loan_date = datetime.strptime(self.date, '%Y-%m-%d').date()
        last_month = loan_date - relativedelta(months=1)
        last_date = str(last_month)
        for rec in self.fund_name.product_pricing:
            if rec.date_from <= last_date <= rec.date_to:
                self.own = self.emp_units_taken * rec.price
                self.company = self.comp_units_taken * rec.price
                self.emp_booster = self.emp_booster_units_taken * rec.price
                self.comp_booster = self.comp_booster_units_taken * rec.price
                self.value_of_units_owned += self.own + self.emp_booster + self.comp_booster + self.company

    @api.onchange('emp_units_taken', 'emp_booster_units_taken', 'comp_booster_units_taken', 'comp_units_taken')
    def constrains_rules(self):
        loan_surr_rules = self.env["loan.rules"].search([('rule_id', '=', 1)])
        # units = self.env['ppf.unit'].search([('name', '=', self.loan_id.employee_name.name)])
        today = datetime.today().date()
        subscription_date = datetime.strptime(self.loan_id.subscription_date, '%Y-%m-%d').date()
        x = today - subscription_date
        x_year = (x.days + x.seconds / 86400) / 365.2
        total_emp_units = 0.0
        total_emp_booster_units = 0.0
        total_comp_booster_units = 0.0
        total_company_units = 0.0
        if self.emp_units_taken:
            for rec in self.loan_id.unit_balance_ids:
                total_emp_units += rec.total_emp_units
                print(total_emp_units)
                if self.emp_units_taken > total_emp_units:
                    raise UserError((
                        'Your Emp Units cant cover the number u entered'))
                else:
                    pass
        if self.emp_booster_units_taken:
            for record in self.loan_id.unit_balance_ids:
                total_emp_booster_units += record.total_emp_booster_units
                if self.emp_booster_units_taken > total_emp_booster_units:
                    raise UserError((
                        'Your Booster Units cant cover the number u entered'))
                else:
                    pass
        if self.comp_booster_units_taken:
            for record in self.loan_id.unit_balance_ids:
                total_comp_booster_units += record.total_comp_booster_units
                if self.comp_booster_units_taken > total_comp_booster_units:
                    raise UserError((
                        'Your Booster Units cant cover the number u entered'))
                else:
                    pass
        if self.comp_units_taken:
            for x in self.loan_id.unit_balance_ids:
                total_company_units += x.total_company_units
                if int(x_year) < 5:
                    if self.comp_units_taken > (total_company_units * loan_surr_rules.third_loan_rule) / 100:
                        raise UserError((
                            'You cant Loan more than 50% of your Company Unites'))
                    else:
                        pass
                else:
                    if self.comp_units_taken > (total_company_units * loan_surr_rules.fourth_loan_rule) / 100:
                        raise UserError((
                            'Your Company Units cant cover the number u entered'))
                    else:
                        pass

    # @api.onchange('comp_units_taken')
    # def onchange_comp_units_taken(self):
    #     rec = self.env['ppf.year.unit.balance'].search([('product', '=', self.fund_name.id),
    #                                                     ('loan_id', '=', self.loan_id.id)])
    #     print(rec)
    #     max_loan_amount = rec.total_company_units * (1/2)
    #     print(max_loan_amount)
    #     if self.comp_units_taken > max_loan_amount:
    #         raise ValidationError("employee can not take more than 50% of company units balance")


class ppfFundPrice(models.Model):
    _name = 'ppf.fund.price'

    fund_name = fields.Many2one('product.template', string='Fund Name', required=True)
    current_price = fields.Float('Current Price', compute='compute_current_price')
    loan_id = fields.Many2one('ppf.loan', string='Loan', ondelete='cascade')

    @api.depends('fund_name')
    @api.one
    def compute_current_price(self):
        loan_date = datetime.strptime(self.loan_id.loan_date, '%Y-%m-%d').date()
        last_month = loan_date - relativedelta(months=1)
        last_date = str(last_month)
        for x in self.fund_name.product_pricing:
            if x.date_from <= last_date <= x.date_to:
                self.current_price = x.price


class AccountInvoiceRelate(models.Model):
    _inherit = 'account.invoice'

    loan_id = fields.Many2one('ppf.loan', string='Loan')
