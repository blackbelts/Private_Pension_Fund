from odoo import models, tools, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError


class ppfSurrender(models.Model):
    _name = 'ppf.surrender'

    employee_name = fields.Many2one('res.partner', string='Employee Name', required=True)
    subscription_date = fields.Date(related='employee_name.subscription_date', string='Subscription Date', store=True,
                                    readonly=True)
    retirement_date = fields.Date('Pension Date', compute='compute_retirement_date')
    total_units_owned = fields.Float('Total Units Owned', readonly=True)
    value_of_units_owned = fields.Float('Surrender Amount', readonly=True)
    surrender_reason = fields.Selection(
        [('normal', 'Normal Reasons'), ('except', 'Exceptions'), ('died', 'Employee Died')], required=True,
        default='normal')
    surrender_date = fields.Date('Surrender Date', default=datetime.today())
    state = fields.Selection(
        [('pending', 'Pending'), ('o/s', 'O/S'), ('inv', 'Units Sold'), ('payable', 'Payable'), ('paid', 'Paid'),
         ('rejected', 'Rejected')],
        required=True, default='pending', compute='_compute_state')
    unit_balance_ids = fields.One2many('ppf.year.unit.balance', 'surrender_id')
    loans_taken_ids = fields.One2many('ppf.loans.taken.before', 'surrender_id')
    surrender_unit_ids = fields.One2many('ppf.surrender.units', 'surrender_id')
    fund_price_ids = fields.One2many('ppf.fund.price', 'surrender_id')
    fund = fields.Many2one('ppf.fund', string='Fund', default=lambda self: self._default_fund())
    loan_allocated = fields.Boolean('is Units Allocated ?', default=False)
    company_share = fields.Float('Company Share', readonly=True)
    employee_share = fields.Float('Employee Share', readonly=True)
    emp_booster = fields.Float('Emp Booster', readonly=True)
    comp_booster = fields.Float('Comp Booster', readonly=True)
    invoice_ids = fields.One2many('account.invoice', 'surrender_id', string='Invoices', readonly=True)
    validate = fields.Boolean('validate ?', default=False)
    invoice_created = fields.Boolean('invoice created ?', default=False)
    bill_created = fields.Boolean('bill created ?', default=False)
    emp_value = fields.Float('Emp Value', readonly=True)
    comp_value = fields.Float('Comp Value', readonly=True)

    # loans_entry_items_ids = fields.One2many('ppf.loan.entry.items', 'surrender_id')

    @api.one
    @api.depends('invoice_ids')
    def _compute_state(self):
        # self.units_allocated = False
        # inv_id = self.env['account.invoice'].search([('loan_id', '=', self.id), ('type', '=', 'in_invoice')])
        unit_id = self.env['ppf.unit'].search([('surrender_id', '=', self.id)])
        sub_id = self.env['ppf.subscription.line'].search([('surrender_id', '=', self.id)])
        self.state = 'pending'
        if self.validate == True:
            self.state = 'o/s'
        for record in self.invoice_ids:
            if record.surrender_id.id == self.id:
                if record.state == 'paid':
                    self.state = 'inv'
                    if unit_id:
                        pass
                    else:
                        for rec in self.surrender_unit_ids:
                            own_units = -rec.total_emp_units
                            company_units = -rec.total_company_units
                            emp_booster_units = -rec.total_emp_booster_units
                            comp_booster_units = -rec.total_comp_booster_units
                            total_units = own_units + company_units + emp_booster_units + comp_booster_units
                            own = -rec.start_own
                            company = -rec.start_company
                            emp_booster = -rec.start_emp_booster
                            comp_booster = -rec.start_comp_booster
                            total_value = own + company + emp_booster + comp_booster
                            self.env['ppf.unit'].create({
                                'name': self.employee_name.id,
                                'member_id': self.employee_name.member_id,
                                'product': rec.product.id,
                                'own_units': own_units,
                                'company_units': company_units,
                                'emp_booster_units': emp_booster_units,
                                'comp_booster_units': comp_booster_units,
                                'total_units': total_units,
                                'own': own,
                                'company': company,
                                'emp_booster': emp_booster,
                                'comp_booster': comp_booster,
                                'total_value': total_value,
                                'date': self.surrender_date,
                                'department': self.employee_name.department.id,
                                'state': 'surrender',
                                'surrender_id': self.id,
                            })
            if self.bill_created == True:
                self.state = 'payable'
            if record.surrender_id.id == self.id and record.type == 'in_invoice':
                if record.state == 'paid':
                    self.state = 'paid'
                    print(111111111)
                    if sub_id:
                        print(2222222222222)
                        pass
                    else:
                        print(33333333333333)
                        for x in self.surrender_unit_ids:
                            print(5555555555555)
                            own = -x.start_own
                            company = -x.start_company
                            emp_booster = -x.start_emp_booster
                            comp_booster = -x.start_comp_booster
                            self.env['ppf.subscription.line'].create({
                                'member_name': self.employee_name.id,
                                'member_id': self.employee_name.member_id,
                                # 'product': rec.fund_name.id,
                                'own': own,
                                'company': company,
                                'emp_booster': emp_booster,
                                'comp_booster': comp_booster,
                                # 'date': datetime.today(),
                                'department': self.employee_name.department.id,
                                'states': 'surrender',
                                'surrender_id': self.id,
                            })

    @api.model
    def _default_fund(self):
        return self.env['ppf.fund'].search([], limit=1)

    # @api.depends('employee_name')
    # @api.multi
    # def compute_sub_date(self):
    #     self.subscription_date = self.employee_name.subscription_date

    @api.depends('employee_name')
    @api.multi
    def compute_retirement_date(self):
        self.retirement_date = self.employee_name.pension_date

    @api.depends('employee_name')
    @api.multi
    def _compute_total_units_owned(self):
        units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name)])
        for rec in units:
            self.total_units_owned += rec.total_units

    @api.depends('employee_name', 'surrender_unit_ids')
    @api.multi
    def _compute_value_units_owned(self):
        if self.subscription_date:
            surr_rules = self.env["loan.rules"].search([('rule_id', '=', 1)])
            surrender_date = datetime.strptime(self.surrender_date, '%Y-%m-%d').date()
            # date = self.subscription_date.str()
            subscription_date = datetime.strptime(self.subscription_date, '%Y-%m-%d').date()
            x = surrender_date - subscription_date
            x_year = (x.days + x.seconds / 86400) / 365.2
            last_month = surrender_date - relativedelta(months=1)
            last_date = str(last_month)
            print(555555555555555555555555555)
            print(int(x_year))
            for rec in self.surrender_unit_ids:
                for x in rec.product.product_pricing:
                    if x.date_from <= last_date <= x.date_to:
                        self.value_of_units_owned += (
                                                             rec.total_emp_units + rec.total_company_units + rec.total_emp_booster_units + rec.total_comp_booster_units) * x.price
                        self.company_share += rec.total_company_units * x.price
                        self.employee_share += rec.total_emp_units * x.price
                        self.emp_booster += rec.total_emp_booster_units * x.price
                        self.comp_booster += rec.total_comp_booster_units * x.price
                        print(rec.total_emp_units)
                        print(x.price)
                        print(self.employee_share)
                        if self.surrender_reason == 'died':
                            self.emp_value = self.value_of_units_owned
                            self.comp_value = 0.0
                        elif self.surrender_reason == 'except':
                            self.emp_value = self.value_of_units_owned
                            self.comp_value = 0.0
                        else:
                            if 0 <= int(x_year) <= 1:
                                self.emp_value = self.employee_share + (
                                        surr_rules.first_surr_rule * self.comp_booster) + (
                                                         surr_rules.first_surr_rule * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value
                            elif 1 <= int(x_year) < 2:
                                self.emp_value = self.employee_share + (
                                        surr_rules.second_surr_rule * self.comp_booster) + (
                                                         surr_rules.second_surr_rule * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value

                            elif 2 <= int(x_year) < 3:
                                self.emp_value = self.employee_share + (
                                        (surr_rules.third_surr_rule / 100) * self.comp_booster) + (
                                                         (
                                                                 surr_rules.third_surr_rule / 100) * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value

                            elif 3 <= int(x_year) < 4:
                                self.emp_value = self.employee_share + (
                                        (surr_rules.fourth_surr_rule / 100) * self.comp_booster) + (
                                                         (
                                                                 surr_rules.fourth_surr_rule / 100) * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value
                            elif 4 <= int(x_year) < 5:
                                self.emp_value = self.employee_share + (
                                        (surr_rules.fifth_surr_rule / 100) * self.comp_booster) + (
                                                         (
                                                                 surr_rules.fifth_surr_rule / 100) * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value
                            elif int(x_year) >= 5:
                                self.emp_value = self.employee_share + (
                                        (surr_rules.final_surr_rule / 100) * self.comp_booster) + (
                                                         (
                                                                 surr_rules.final_surr_rule / 100) * self.company_share) + self.emp_booster
                                self.comp_value = self.value_of_units_owned - self.emp_value

    @api.depends('surrender_unit_ids')
    @api.multi
    def approve(self):
        data = []
        for rec in self.surrender_unit_ids:
            z = (0, 0, {
                'name': 'Invoice For Surrender',
                'quantity': 1,
                'product_id': rec.product.id,
                'price_unit': self.value_of_units_owned,
                'account_id': rec.product.property_account_income_id.id,
            })
            data.append(z)
        bill = self.env['account.invoice'].create({
            'type': 'out_invoice',
            'partner_id': self.employee_name.department.id,
            'user_id': self.env.user.id,
            'surrender_id': self.id,
            'origin': 'surrender',
            'date_invoice': self.surrender_date,
            'date_due': self.surrender_date,
            'invoice_line_ids': data,
        })
        self.invoice_created = True

        # bill.action_invoice_open()
        # self.state = 'paid'
        # for rec in self.surrender_unit_ids:
        #     own = - rec.total_emp_units
        #     company = rec.total_company_units
        #     booster = rec.total_booster_units
        #     self.env['ppf.unit'].create({
        #         'name': self.employee_name.name,
        #         'product': rec.product.id,
        #         'own_units': own,
        #         'company_units': company,
        #         'booster_units': booster,
        #         'date': datetime.today(),
        #         'department': self.employee_name.department.id,
        #         'state': 'surrender',
        #         'surrender_id': self.id,
        #     })

    @api.multi
    def create_customer_bill(self):
        inv_id = self.env['account.invoice'].search([('surrender_id', '=', self.id), ('type', '=', 'in_invoice')])
        if self.state == 'inv':
            if inv_id:
                pass
            else:
                data = []
                for rec in self.surrender_unit_ids:
                    z = (0, 0, {
                        'name': 'Invoice For Surrender',
                        'quantity': 1,
                        'product_id': rec.product.id,
                        'price_unit': self.emp_value,
                        'account_id': self.employee_name.property_account_payable_id.id,
                    })
                    data.append(z)
                self.env['account.invoice'].create({
                    'type': 'in_invoice',
                    'partner_id': self.employee_name.id,
                    'user_id': self.env.user.id,
                    'surrender_id': self.id,
                    'origin': 'Surrender',
                    # 'state': 'open',
                    'date_invoice': self.surrender_date,
                    'date_due': self.surrender_date,
                    'invoice_line_ids': data,
                })
                if self.comp_value:
                    data = []
                    for rec in self.surrender_unit_ids:
                        z = (0, 0, {
                            'name': 'Invoice For Surrender',
                            'quantity': 1,
                            'product_id': rec.product.id,
                            'price_unit': self.comp_value,
                            'account_id': self.employee_name.department.property_account_payable_id.id,
                        })
                        data.append(z)
                    self.env['account.invoice'].create({
                        'type': 'in_invoice',
                        'partner_id': self.employee_name.department.id,
                        'user_id': self.env.user.id,
                        'surrender_id': self.id,
                        'origin': 'Surrender',
                        # 'state': 'open',
                        'date_invoice': self.surrender_date,
                        'date_due': self.surrender_date,
                        'invoice_line_ids': data,
                    })

                self.bill_created = True
                self.employee_name.active = False

    def reject(self):
        self.state = 'rejected'

    # @api.model_cr
    @api.one
    def compute_total_emp_units(self):
        funds = self.env['product.template'].search([])
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name), ('product', '=', fund.id)])
            seen = []
            fund_seen = []
            seens = []
            loans_Seen = []
            own = 0.0
            company = 0.0
            emp_booster = 0.0
            comp_booster = 0.0
            for rec in units:
                x = datetime.strptime(rec.date, '%Y-%m-%d').year
                print(x)

                if rec.product in seen:
                    print('duplicated')
                else:
                    own += rec.own_units
                    company += rec.company_units
                    emp_booster += rec.emp_booster_units
                    comp_booster += rec.comp_booster_units

                    self.env.cr.execute("INSERT INTO  ppf_year_unit_balance (year, product, total_emp_units,"
                                        "total_company_units,total_emp_booster_units,total_comp_booster_units, "
                                        "surrender_id) VALUES (%s,%s,%s,%s,%s,%s,%s) "
                                        , (datetime.strptime(rec.date, '%Y-%m-%d').year, rec.product.id, own
                                           , company, emp_booster, comp_booster, self.id))
                    seen.append(rec.product)

        loans = self.env['ppf.loan'].search([('employee_name', '=', self.employee_name.name),
                                             ('state', '=', 'paid')])

        for record in loans:
            if record in loans_Seen:
                print('duplicated')
            else:
                for rec in record.loans_taken_ids:
                    self.env.cr.execute("INSERT INTO  ppf_loans_taken_before (date, fund_name, emp_units_taken,"
                                        "comp_units_taken, booster_emp_units_taken,booster_comp_units_taken, "
                                        "surrender_id) VALUES (%s,%s,%s,%s,%s,%s) "
                                        , (rec.date, rec.fund_name.id, rec.emp_units_taken, rec.comp_units_taken,
                                           rec.booster_emp_units_taken, rec.booster_comp_units_taken, self.id))
                    loans_Seen.append(record)

        funds = self.env['product.template'].search([])
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name),
                                                 ('product', '=', fund.id)])
            for unit in units:
                if unit:
                    if unit.product in fund_seen:
                        print('duplicated')
                    else:
                        self.env.cr.execute("INSERT INTO  ppf_fund_price (fund_name, surrender_id) VALUES (%s,%s)"
                                            , (unit.product.id, self.id))
                        fund_seen.append(unit.product)

        surrender_date = datetime.strptime(self.surrender_date, '%Y-%m-%d').date()
        subscription_date = datetime.strptime(self.subscription_date, '%Y-%m-%d').date()
        x = surrender_date - subscription_date
        x_year = (x.days + x.seconds / 86400) / 365.2
        print(x_year)
        funds = self.env['product.template'].search([])
        print(funds)
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', self.employee_name.name), ('product', '=', fund.id)])
            print(units)
            own_unit = 0.0
            company_units = 0.0
            emp_booster_unit = 0.0
            comp_booster_unit = 0.0
            start_own = 0.0
            start_company = 0.0
            start_emp_booster = 0.0
            start_comp_booster = 0.0

            if units:
                # if self.surrender_reason == 'died':
                for recor in units:
                    print(22222)
                    own_unit += recor.own_units
                    emp_booster_unit += recor.emp_booster_units
                    comp_booster_unit += recor.comp_booster_units
                    company_units += recor.company_units
                    start_own += recor.own
                    start_company += recor.company
                    start_emp_booster += recor.emp_booster
                    start_comp_booster += recor.comp_booster
                    print(own_unit, company_units)
                self.env.cr.execute("INSERT INTO ppf_surrender_units (duration, product, total_emp_units,"
                                    "total_company_units,total_emp_booster_units,total_comp_booster_units,start_own,"
                                    "start_company,start_emp_booster,start_comp_booster, "
                                    "surrender_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
                                    , (int(x_year), recor.product.id, own_unit
                                       , company_units, emp_booster_unit, comp_booster_unit, start_own, start_company,
                                       start_emp_booster, start_comp_booster, self.id))
            self.loan_allocated = True
            self.validate = True
        self._compute_value_units_owned()

    # def test_surr(self):
    #     self.get_emp_expected_surr(29,'2020-09-01')

    @api.model
    def get_emp_expected_surr(self, user_id, date):
        # user_id = 31
        # date = '2019-12-03'
        partner = self.env['res.users'].search([('id', '=', user_id)]).partner_id
        print(partner)
        funds = self.env['product.template'].search([])
        total_emp_value = 0.0
        total_comp_value = 0.0
        all_units = 0.0
        for fund in funds:
            units = self.env['ppf.unit'].search([('name', '=', partner.name), ('product', '=', fund.id)])
            print(units)
            own_units = 0.0
            company_units = 0.0
            emp_booster_units = 0.0
            comp_booster_units = 0.0
            if units:
                for recor in units:
                    print(22222)
                    own_units += recor.own_units
                    emp_booster_units += recor.emp_booster_units
                    comp_booster_units += recor.comp_booster_units
                    company_units += recor.company_units
            total_units = own_units + company_units + emp_booster_units + comp_booster_units
            all_units += total_units
            surr_rules = self.env["loan.rules"].search([('rule_id', '=', 1)])
            surrender_date = datetime.strptime(date, '%Y-%m-%d').date()
            # date = self.subscription_date.str()
            subscription_date = datetime.strptime(partner.subscription_date, '%Y-%m-%d').date()
            x = surrender_date - subscription_date
            x_year = (x.days + x.seconds / 86400) / 365.2
            last_month = surrender_date - relativedelta(months=1)
            last_date = str(last_month)
            value_of_units_owned = 0.0
            company_share = 0.0
            emp_booster = 0.0
            comp_booster = 0.0
            employee_share = 0.0
            emp_value = 0.0
            comp_value = 0.0
            for x in fund.product_pricing:
                if x.date_from <= last_date <= x.date_to:
                    value_of_units_owned += (
                                                    own_units + emp_booster_units + comp_booster_units + company_units) * x.price
                    company_share += company_units * x.price
                    employee_share += own_units * x.price
                    emp_booster += emp_booster_units * x.price
                    comp_booster += comp_booster_units * x.price
                    if 0 <= int(x_year) < 1:
                        emp_value = employee_share + (
                                surr_rules.first_surr_rule * comp_booster) + (
                                            surr_rules.first_surr_rule * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value
                    elif 1 <= int(x_year) < 2:
                        emp_value = employee_share + (
                                surr_rules.second_surr_rule * comp_booster) + (
                                            surr_rules.second_surr_rule * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value

                    elif 2 <= int(x_year) < 3:
                        emp_value = employee_share + (
                                (surr_rules.third_surr_rule / 100) * comp_booster) + (
                                            (
                                                    surr_rules.third_surr_rule / 100) * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value

                    elif 3 <= int(x_year) < 4:
                        emp_value = employee_share + (
                                (surr_rules.fourth_surr_rule / 100) * comp_booster) + (
                                            (
                                                    surr_rules.fourth_surr_rule / 100) * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value
                    elif 4 <= int(x_year) < 5:
                        emp_value = employee_share + (
                                (surr_rules.fifth_surr_rule / 100) * comp_booster) + (
                                            (
                                                    surr_rules.fifth_surr_rule / 100) * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value
                    elif int(x_year) >= 5:
                        emp_value = employee_share + (
                                (surr_rules.final_surr_rule / 100) * comp_booster) + (
                                            (
                                                    surr_rules.final_surr_rule / 100) * company_share) + emp_booster
                        comp_value = value_of_units_owned - emp_value
            total_emp_value += emp_value
            total_comp_value += comp_value
        all_value = total_comp_value + total_emp_value
        print(all_value, all_units)
        return [{'all_units': all_units,
                 'all_value': all_value}]


# 'employee_share': employee_share, 'company_share': company_share, 'emp_booster': emp_booster,
#                  'comp_booster': comp_booster, 'own_units': own_units, 'comp_units': company_units,
#                  'emp_booster_units': emp_booster_units, 'comp_booster_units': comp_booster_units,
#                  'total_units': total_units,
#                  'emp_value': emp_value, 'comp_value': comp_value,

class ppfYearUnitBalance(models.Model):
    _inherit = 'ppf.year.unit.balance'

    surrender_id = fields.Many2one('ppf.surrender', string='Surrender', ondelete='cascade')


class ppfLoansTakenBefore(models.Model):
    _inherit = 'ppf.loans.taken.before'

    surrender_id = fields.Many2one('ppf.surrender', string='Surrender', ondelete='cascade')


class ppfSurrenderUnits(models.Model):
    _name = 'ppf.surrender.units'

    duration = fields.Integer('He Surrender After')
    product = fields.Many2one('product.template', string='Product')
    total_emp_units = fields.Float('Total Emp.Units')
    total_company_units = fields.Float('Total Comp.Units')
    total_emp_booster_units = fields.Float('Total Emp Booster Units')
    total_comp_booster_units = fields.Float('Total Comp Booster Units')
    total = fields.Float('Total', compute='compute_total')
    surrender_id = fields.Many2one('ppf.surrender', string='Surrender', ondelete='cascade')
    own = fields.Float('Emp Units Value', compute='compute_values')
    company = fields.Float('Comp Units Value', compute='compute_values')
    emp_booster = fields.Float('Emp Booster Value', compute='compute_values')
    comp_booster = fields.Float('Comp Booster Value', compute='compute_values')
    value_of_units_owned = fields.Float('Own Units Value', compute='compute_values')
    start_own = fields.Float('Emp Units Value')
    start_company = fields.Float('Comp Units Value')
    start_emp_booster = fields.Float('Emp Booster Value')
    start_comp_booster = fields.Float('Comp Booster Value')
    total_start = fields.Float('Total Start', compute='compute_total')

    @api.one
    @api.depends('total_emp_units', 'total_emp_booster_units', 'total_comp_booster_units', 'total_company_units')
    def compute_total(self):
        self.total = self.total_company_units + self.total_emp_units + self.total_emp_booster_units + self.total_comp_booster_units
        self.total_start = self.start_own + self.start_company + self.start_emp_booster + self.start_comp_booster

    @api.one
    @api.depends('total_emp_units', 'total_comp_booster_units', 'total_emp_booster_units', 'total_company_units')
    def compute_values(self):
        for x in self.product.product_pricing:
            if x.date_from <= self.surrender_id.surrender_date <= x.date_to:
                self.own = self.total_emp_units * x.price
                self.company = self.total_company_units * x.price
                self.emp_booster = self.total_emp_units * x.price
                self.comp_booster = self.total_company_units * x.price
                self.value_of_units_owned += self.own + self.total_emp_units + self.company + self.total_company_units


class ppfFundPrice(models.Model):
    _inherit = 'ppf.fund.price'

    surrender_id = fields.Many2one('ppf.surrender', string='Surrender', ondelete='cascade')

    @api.depends('fund_name')
    @api.one
    def compute_current_price(self):
        if self.surrender_id:
            for x in self.fund_name.product_pricing:
                if x.date_from <= self.surrender_id.surrender_date <= x.date_to:
                    self.current_price = x.price


class AccountInvoiceRelate(models.Model):
    _inherit = 'account.invoice'

    surrender_id = fields.Many2one('ppf.surrender', string='Surrender')
