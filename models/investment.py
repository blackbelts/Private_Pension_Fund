from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime


class ppfInvestment(models.Model):
    _name = 'ppf.investment'

    @api.model
    def _default_currency(self):
        return self.env.user.company_id.currency_id

    name = fields.Char(string='Name', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    state = fields.Selection([('draft', 'Draft'), ('open', 'Invoiced'), ('paid', 'Paid')],
                             required=True, default='draft', compute='_compute_state', search='compute_unit')
    company = fields.Many2one('res.partner', string='Asset Manager', required=True, domain="[('customer','=',False)]")
    invested_date = fields.Date('Investment Date', default=datetime.today())
    total_amount = fields.Float(store=True, readonly=True, digits=(16, 4), string='Total Invested',
                                compute='_compute_amount')
    cash_pool_id = fields.Many2one('ppf.cash.pool', string="Cash Pool", domain="[('state','=','o/s')]")
    # trx_id = fields.Many2one('cash.pool.trans', string="Cash Pool")
    product = fields.Many2one('product.template', related='cash_pool_id.product', string='Investment Product')

    cash_pool_amount = fields.Float(related='cash_pool_id.amount', digits=(16, 4), string='Cash Pool Amount')
    cash_poo_perv_amount = fields.Float(related='cash_pool_id.perv_amount', string='Previous Invested')
    cash_pool_os_amount = fields.Float(related='cash_pool_id.os_amount', digits=(16, 4), string='Outstanding')
    type_categ = fields.Many2one(related='cash_pool_id.type', store=True)
    investment_line_ids = fields.One2many('ppf.investment.line', 'investment_id')
    invoice_ids = fields.One2many('account.invoice', 'investment_id', string='Invoices', readonly=True)
    validate_cash_pool = fields.Boolean('', default=True)
    validate_bills_button = fields.Boolean('', default=False)
    validate_bills = fields.Boolean('', default=False)
    created_bill = fields.Boolean('', default=False)

    currency_id = fields.Many2one('res.currency', string='Currency',
                                  required=True, readonly=True,
                                  default=_default_currency, track_visibility='always')

    validate_invest_lines = fields.Boolean('')
    unit = fields.One2many('ppf.unit', 'investment_id', string='Unit')
    units_allocated = fields.Boolean('is Units Allocated ?', default=True)
    # allocated = fields.Boolean('is Units Allocated ?', default=False)
    unit_price = fields.Float(digits=(16, 4), string='Unit Price', compute='_compute_unit_price')
    amount_of_units = fields.Float(digits=(16, 4), string='Amount of Units', compute='_compute_amout_of_units')

    @api.one
    @api.depends('unit_price')
    def _compute_amout_of_units(self):
        if self.unit_price:
            self.amount_of_units = self.cash_pool_os_amount / self.unit_price

    @api.one
    @api.depends('product', 'invested_date')
    def _compute_unit_price(self):
        for rec in self.product.product_pricing:
            # today = datetime.today().strftime('%Y-%m-%d')
            if rec.date_from <= self.invested_date and self.invested_date <= rec.date_to:
                self.unit_price = rec.price

    @api.multi
    def validate_cash(self):
        self.validate_cash_pool = True
        self.validate_invest_lines = False
        return True

    @api.multi
    def validate_Lines(self):
        self.validate_cash_pool = False
        self.validate_invest_lines = True
        return True

    @api.multi
    def validate_bill(self):
        self.validate_cash_pool = False
        self.validate_invest_lines = False
        self.validate_bills = True

        return True

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('ppf.investment') or 'New'
        return super(ppfInvestment, self).create(vals)

    # @api.multi
    # def validate(self):
    #     if self.product:
    #         self.state = 'open'
    #     else:
    #         raise ValidationError(_('Please create some Investment Lines'))

    @api.one
    @api.depends('investment_line_ids')
    def _compute_amount(self):
        self.total_amount = self.cash_pool_os_amount

    @api.multi
    def create_bill(self):
        self.cash_pool_id.state = 'inv'
        self.env['account.invoice'].create({
            'type': 'in_invoice',
            'partner_id': self.company.id,
            'user_id': self.company.user_id.id,
            'investment_id': self.id,
            'origin': self.name,
            # 'state': 'open',
            'journal_id': 1,
            'date_due': self.invested_date,
            'invoice_line_ids': [(0, 0, {
                'name': 'Bill For Investment',
                'quantity': 1,
                'product_id': self.product.id,
                'price_unit': self.cash_pool_os_amount,
                'account_id': self.product.property_account_expense_id.id,
            })],
        })
        # bill.invoice_validate()
        # self.state = 'paid'
        # self.state = 'open'
        self.created_bill = True
        self.validate_bills_button = True

    @api.multi
    def invest_print(self):
        return self.env.ref('Private_Pension_Fund.invest').report_action(self)

    @api.multi
    def send_mail_invest(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('Private_Pension_Fund.invest_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'ppf.investment',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    @api.depends('invoice_ids')
    def _compute_state(self):
        # self.units_allocated = False
        self.state = 'draft'
        if self.created_bill == True:
            self.state = 'open'
        for record in self.invoice_ids:
            if record.investment_id.id == self.id:
                # if self.allocated == False:
                if record.state == 'paid':
                    self.state = 'paid'
                    self.cash_pool_id.state = 'inv'
                    res = []
                    own = 0.0
                    company = 0.0
                    booster = 0.0
                    own_units = 0.0
                    company_units = 0.0
                    booster_units = 0.0
                    self.units_allocated = False
                    department = self.cash_pool_id.subscription_id.department.id
                    inv_id = self.env['ppf.unit'].search([('investment_id', '=', self.id)])
                    product = self.product
                    # rate = self.total_amount / self.cash_pool_id.subscription_id.total_amount
                    if self.units_allocated == False:
                        for rec in self.cash_pool_id.subscription_id.subscription_line:
                            unit_price = 0.0
                            for x in self.product.product_pricing:
                                # today = datetime.today().strftime('%Y-%m-%d')
                                if x.date_from <= self.invested_date <= x.date_to:
                                    unit_price = x.price
                            own = rec.own
                            company = rec.company
                            emp_booster = rec.emp_booster
                            comp_booster = rec.comp_booster
                            own_units = (rec.own / unit_price)
                            company_units = (rec.company / unit_price)
                            emp_booster_units = (rec.emp_booster / unit_price)
                            comp_booster_units = (rec.comp_booster / unit_price)
                            total = own_units + company_units + emp_booster_units + comp_booster_units
                            total_value = own + company + emp_booster + comp_booster
                            member_id = rec.member_name.member_id

                            res.append(
                                {"name": rec.member_name.id, "own": own, "own_units": own_units, "company": company
                                    , "company_units": company_units, "emp_booster": emp_booster,
                                 "comp_booster": comp_booster, 'member_id': member_id,
                                 "emp_booster_units": emp_booster_units, "comp_booster_units": comp_booster_units,
                                 "total_units": total, "total_value": total_value,
                                 "product": product})
                            # print(res)
                        if inv_id:
                            pass
                        else:
                            for x in res:
                                self.env['ppf.unit'].create({
                                    'name': x['name'],
                                    'member_id': x['member_id'],
                                    'product': product.id,
                                    'own_units': x['own_units'],
                                    'company_units': x['company_units'],
                                    'emp_booster_units': x['emp_booster_units'],
                                    'comp_booster_units': x['comp_booster_units'],
                                    'department': department,
                                    'investment_id': self.id,
                                    'date': self.invested_date,
                                    'own': x['own'],
                                    'company': x['company'],
                                    'emp_booster': x['emp_booster'],
                                    'comp_booster': x['comp_booster'],
                                    'total_units': x['total_units'],
                                    'total_value': x['total_value'],
                                })
                            self.units_allocated = True
                        # self.allocated = True
                        # return {
                        #     'name': ('Unit'),
                        #     'view_type': 'form',
                        #     'view_mode': 'tree',
                        #     'res_model': 'ppf.unit',
                        #     'view_id': (self.env.ref('Private_Pension_Fund.unit_tree').id),
                        #     'type': 'ir.actions.act_window',
                        #     'target': 'current',
                        # }

    # @api.multi
    # @api.onchange('state')
    # def compute_unit(self):
    #     if self.state == 'paid':


class investmentLine(models.Model):
    _name = 'ppf.investment.line'

    type_line = fields.Many2one(related='investment_id.type_categ')
    product = fields.Many2one('product.template', string='Investment Fund')
    quantity = fields.Integer('Units', compute='_compute_quantity')
    unit_price = fields.Float('Unit Price', compute='_compute_unit_price')
    invest_amount = fields.Float('Invest Amount')
    amount = fields.Float('Amount', compute='_compute_amount')
    investment_id = fields.Many2one('ppf.investment')
    currency_id = fields.Many2one(related='investment_id.currency_id')

    @api.one
    @api.depends('product')
    def _compute_unit_price(self):
        for rec in self.product.product_pricing:
            # today = datetime.today().strftime('%Y-%m-%d')
            if rec.date_from <= self.investment_id.invested_date and self.investment_id.invested_date <= rec.date_to:
                self.unit_price = rec.price

    @api.one
    @api.depends('unit_price', 'invest_amount')
    def _compute_amount(self):
        self.amount = self.quantity * self.unit_price

    @api.one
    @api.depends('invest_amount')
    def _compute_quantity(self):
        if self.unit_price != 0:
            self.quantity = self.invest_amount / self.unit_price


class AccountInvoiceRelate(models.Model):
    _inherit = 'account.invoice'

    investment_id = fields.Many2one('ppf.investment', string='Investment')
