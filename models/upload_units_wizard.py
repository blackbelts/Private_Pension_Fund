from odoo import models, fields, api
from datetime import datetime
from odoo import xlrd
from xlrd import open_workbook
import base64


class Wizard(models.TransientModel):
    _name = 'ppf.units.wizard'

    def _default_session(self):
        return self.env['ppf.units.wizard'].browse(self._context.get('active_id'))

    state = fields.Selection([('open', 'Open'), ('loan', 'Loan'), ('surrender', 'Surrender')],
                             required=True, default='open', string='State')
    date = fields.Date('Allocation Date', default=datetime.today())
    department = fields.Many2one('res.partner', string='Subsidiary', domain="[('is_subcompany','=',True)]")
    data = fields.Binary('File')

    @api.model_cr
    @api.multi
    def upload_file(self):
        wb = open_workbook(file_contents=base64.decodestring(self.data))
        sheet = wb.sheets()[0]

        for s in wb.sheets():

            values = []

            for row in range(1, s.nrows):

                col_value = []

                for col in range(s.ncols):
                    value = (s.cell(row, col).value)

                    try:
                        value = str(int(value))
                        value = float(value)
                    except:
                        pass

                    col_value.append(value)
                print('20202020202020')
                print(col_value)
                name = self.env['res.partner'].search([('name', '=', col_value[0])]).name
                product = self.env['product.template'].search([('name', '=', col_value[1])]).id
                self.env.cr.execute("INSERT INTO  ppf_unit (name, state, department, product, own_units,"
                                    " company_units, booster_units) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                                    , (name, self.state, self.department.id, product, col_value[2], col_value[3],
                                       col_value[4]))
                # self.update({
                # 'subscription_line': [(0, 0, {'member_name':(self.env['res.partner'].search([('name', '=',col_value[0])]).id), 'salary': col_value[1],'perc_salary': col_value[2],
                #                               'own':col_value[3],'company':col_value[4],'booster':col_value[5]})],
                # })

            values.append(col_value)
        print(values)
