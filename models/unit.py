from odoo import models, tools, fields, api
from datetime import datetime
from odoo import xlrd
from xlrd import open_workbook
import base64


class ppfUnit(models.Model):
    _name = 'ppf.unit'

    name = fields.Many2one('res.partner', 'Employee Name')
    member_id = fields.Char('Member ID')
    product = fields.Many2one('product.template', string='Investment Fund')
    own = fields.Float('Emp Value At Cost', digits=(16, 4))
    company = fields.Float('Comp Value At Cost', digits=(16, 4))
    emp_booster = fields.Float('Emp Booster Value At Cost', digits=(16, 4))
    comp_booster = fields.Float('Comp Booster Value At Cost', digits=(16, 4))
    own_units = fields.Float('Emp Units', digits=(16, 4))
    company_units = fields.Float('Comp Units', digits=(16, 4))
    emp_booster_units = fields.Float('Emp Booster Units', digits=(16, 4))
    comp_booster_units = fields.Float('Comp Booster Units', digits=(16, 4))
    date = fields.Date('Allocation Date')
    department = fields.Many2one('res.partner', string='Subsidiary', domain="[('is_subcompany','=',True)]")
    subscription_id = fields.Many2one('ppf.subscription', string='Subscription ID')
    investment_id = fields.Many2one('ppf.investment')
    total_units = fields.Float('Total Units', digits=(16, 4))
    total_value = fields.Float('Total Value At Cost', digits=(16, 4))
    state = fields.Selection([('open', 'Open'), ('loan', 'Loan'), ('surrender', 'Surrender')],
                             required=True, default='open', string='State')
    loan_id = fields.Many2one('ppf.loan', string='Loan')
    surrender_id = fields.Many2one('ppf.surrender', string='Surrender')
    department2 = fields.Many2one('ppf.department', string='Units')

    today_own_units_value = fields.Float('Emp Today Value', digits=(16, 4), compute='_compute_today_values',
                                         search='_search_upper', store=True)
    today_comp_units_value = fields.Float('Comp Today Value', digits=(16, 4), compute='_compute_today_values',
                                          search='_search_upper', store=True)

    today_emp_booster_units_value = fields.Float('Emp Booster Today Value', digits=(16, 4),
                                                 compute='_compute_today_values',
                                                 search='_search_upper', store=True)
    today_comp_booster_units_value = fields.Float('Comp Booster Today Value', digits=(16, 4),
                                                  compute='_compute_today_values',
                                                  search='_search_upper', store=True)

    today_total_value = fields.Float('Total Today Value', digits=(16, 4), compute='_compute_today_values',
                                     search='_search_upper', store=True)

    profit_or_lost = fields.Float('P/L', digits=(16, 4), compute='_compute_today_values',
                                  search='_search_upper', store=True)

    # rate = fields.Float('ROI%', compute='_compute_today_values', store=False)

    def _search_upper(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        return [('name', operator, value)]

    @api.one
    @api.depends('product.product_pricing')
    def _compute_today_values(self):
        today = datetime.today().strftime('%Y-%m-%d')
        if self.product:
            for rec in self.product.product_pricing:
                if rec.date_from <= today and today <= rec.date_to:
                    self.today_own_units_value = rec.price * self.own_units
                    self.today_comp_units_value = rec.price * self.company_units
                    self.today_emp_booster_units_value = rec.price * self.emp_booster_units
                    self.today_comp_booster_units_value = rec.price * self.comp_booster_units
                    self.today_total_value = self.today_emp_booster_units_value + self.today_comp_booster_units_value + self.today_comp_units_value + self.today_own_units_value
                    self.profit_or_lost = self.today_total_value - self.total_value
            # self.rate = (self.profit_or_lost / self.total_value) * 100

    @api.one
    def _set_total(self):

        return True

    # @api.onchange('today_own_units_value', 'today_comp_units_value', 'today_booster_units_value', 'today_total_value',
    #               'profit_or_lost')
    # def set_values(self):
    #     self.new_own_unit_value = self.today_own_units_value
    #     self.new_company_unit_value = self.today_comp_units_value
    #     self.new_booster_unit_value = self.today_booster_units_value
    #     self.new_total_value = self.today_total_value
    #     self.new_profit_or_lose = self.profit_or_lost

# class UnitsWizard(models.TransientModel):
#     _name = 'ppf.units.values.wizard'
#
#     @api.multi
#     def get_total_values(self):
#         x = self.env['ppf.unit'].read_group(([]),
#                                             ['name', 'own', 'company', 'booster', 'total_value',
#                                              'today_own_units_value', 'today_comp_units_value',
#                                              'today_booster_units_value', 'today_total_value', 'profit_or_lost',
#                                              'rate'], ['name'], offset=0,
#                                             limit=None, orderby=False, lazy=True)
#         return {
#             'name': ('Employee Units Values'),
#             'view_type': 'form',
#             'view_mode': 'tree',
#             'res_model': 'ppf.unit',
#             'view_id': self.env.ref('Private_Pension_Fund.unit_tree_values').id,
#             'target': 'current',
#             'type': 'ir.actions.act_window',
#             'domain': ([('id', 'in', x)])
#
#         }
#         print(x)
