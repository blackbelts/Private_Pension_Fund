import io
from datetime import timedelta, datetime
import xlsxwriter
import xlwt
from xlsxwriter.workbook import Workbook
import base64
import xlrd
from xlrd import open_workbook
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo import api, fields, tools, models, _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


class fund_wizard(models.Model):
    _name = 'ppf.units.report'
    date_from = fields.Date(string="From Date", required=True, default=datetime.today())
    to_date = fields.Date(string="To Date", required=True, default=datetime.today())

    @api.multi
    def create_pivot_report(self):
        domain = [('date', '>=', self.date_from),
                  ('date', '<=', self.to_date), ]
        print(domain)
        return {
            'name': ('Report'),
            'view_type': 'pivot',
            'view_mode': 'pivot',
            'res_model': 'ppf.unit',
            'view_id': [(self.env.ref('Private_Pension_Fund.fund_pivot').id), 'pivot'],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': domain,

        }


class PivotReportCRM(models.Model):
    _name = "ppf.pivot.report"
    _table = 'ppf_pivot_report'
    _auto = False

    name = fields.Many2one('res.partner', 'Employee Name', readonly=True)
    own = fields.Float('Emp Value At Cost', digits=(16, 4), readonly=True)
    company = fields.Float('Comp Value At Cost', digits=(16, 4), readonly=True)
    emp_booster = fields.Float('Emp Booster Value', digits=(16, 4), readonly=True)
    comp_booster = fields.Float('Comp Booster Value', digits=(16, 4), readonly=True)
    own_units = fields.Float('Emp Share', digits=(16, 4), readonly=True)
    company_units = fields.Float('Comp Share', digits=(16, 4), readonly=True)
    emp_booster_units = fields.Float('Emp Booster', digits=(16, 4), readonly=True)
    comp_booster_units = fields.Float('Comp Booster', digits=(16, 4), readonly=True)
    date = fields.Date('Allocation Date', default=datetime.today(), readonly=True)
    total_units = fields.Float('Total Units', digits=(16, 4), readonly=True)
    total_value = fields.Float('Total Value At Cost', digits=(16, 4), readonly=True)
    today_own_units_value = fields.Float('Today Emp Value', digits=(16, 4), readonly=True)
    today_comp_units_value = fields.Float('Today Comp Value', digits=(16, 4), readonly=True)
    today_emp_booster_units_value = fields.Float('Today Emp Booster Value', digits=(16, 4), readonly=True)
    today_comp_booster_units_value = fields.Float('Today Comp Booster Value', digits=(16, 4), readonly=True)
    today_total_value = fields.Float('Today Total Value', digits=(16, 4), readonly=True)
    profit_or_lost = fields.Float('P/L', digits=(16, 4), readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        query = """CREATE VIEW ppf_pivot_report AS ( SELECT id,name,own,company,emp_booster,comp_booster, own_units,company_units,
        emp_booster_units,comp_booster_units,date,total_units,total_value,today_own_units_value,today_comp_units_value,
        today_emp_booster_units_value,today_comp_booster_units_value,today_total_value,profit_or_lost from ppf_unit group by id,name,date) """
        self.env.cr.execute(query)


class UnitsExecl(models.Model):
    _name = "unit.excel.report"

    to_date = fields.Date(string="To Date", required=True, default=datetime.today())
    product = fields.Many2one('product.template', 'Products', required=True)
    currency_rate = fields.Float('Dollar Rate')
    is_confirmed = fields.Boolean(string="", compute='is_confirm')
    my_file = fields.Binary(string="Get Your File")
    excel_sheet_name = fields.Char(string='Name', size=64)

    @api.one
    @api.depends('my_file')
    def is_confirm(self):
        if self.my_file:
            self.is_confirmed = True
        return True

    @api.multi
    def generate_units_report_excel(self):
        data = []

        if self.to_date:
            emp_units = 0.0
            comp_units = 0.0
            emp_booster_units = 0.0
            comp_booster_units = 0.0
            total_units = 0.0
            emp_value = 0.0
            comp_value = 0.0
            emp_booster_value = 0.0
            comp_booster_value = 0.0
            total_value = 0.0
            price = 0.0
            for x in self.product.product_pricing:
                if x.date_from <= self.to_date <= x.date_to:
                    price = x.price
                    print(price)
            employee = self.env["res.partner"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('department', '=', 'Azimut')])
            for rec in employee:
                for record in self.env["ppf.unit"].search(
                        [('date', '<=', self.to_date), ('name', '=', rec.name), ('product', '=', self.product.id)]):
                    emp_units += record.own_units
                    comp_units += record.company_units
                    emp_booster_units += record.emp_booster_units
                    comp_booster_units += record.comp_booster_units
                    total_units += record.total_units
                emp_value = emp_units * price
                comp_value = comp_units * price
                emp_booster_value = emp_booster_units * price
                comp_booster_value = comp_booster_units * price
                total_value = total_units * price
                x = {rec.name: [emp_units, comp_units, emp_booster_units, comp_booster_units, total_units, emp_value,
                                comp_value, emp_booster_value, comp_booster_value, total_value]}
                data.append(x)

        self.excel_sheet_name = 'report_task.xlsx'
        # s=str(self.path)+'/'+'installment.xlsx'
        # print(s)
        self.excel_sheet_name = 'Units.xlsx'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet = workbook.add_worksheet()
        # Widen the first column to make the text clearer.
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        worksheet.set_column('J:J', 15)
        worksheet.set_column('K:K', 15)

        worksheet.write('A1', 'Employee Name')
        worksheet.write('B1', 'Emp Units')
        worksheet.write('C1', 'Comp Units')
        worksheet.write('D1', 'Emp Booster Units')
        worksheet.write('E1', 'Comp Booster Units')
        worksheet.write('F1', 'Total Units')
        worksheet.write('G1', 'Emp Value')
        worksheet.write('H1', 'Comp Value')
        worksheet.write('I1', 'Emp Booster Value')
        worksheet.write('J1', 'Comp Booster Value')
        worksheet.write('K1', 'Total Value')

        i = 1
        col = 0
        for record in data:
            worksheet.write(i, col, str(list(record.keys())[0]))
            worksheet.write_number(i, col + 1, float(list(record.values())[0][0]))
            worksheet.write_number(i, col + 2, float(list(record.values())[0][1]))
            worksheet.write_number(i, col + 3, float(list(record.values())[0][2]))
            worksheet.write_number(i, col + 4, float(list(record.values())[0][3]))
            worksheet.write_number(i, col + 5, float(list(record.values())[0][4]))
            worksheet.write_number(i, col + 6, float(list(record.values())[0][5]))
            worksheet.write_number(i, col + 7, float(list(record.values())[0][6]))
            worksheet.write_number(i, col + 8, float(list(record.values())[0][7]))
            worksheet.write_number(i, col + 9, float(list(record.values())[0][8]))
            worksheet.write_number(i, col + 10, float(list(record.values())[0][9]))

            i += 1
        workbook.close()

        output.seek(0)
        self.write({'my_file': base64.encodestring(output.getvalue())})
        return {
            "type": "ir.actions.do_nothing",
        }

    @api.multi
    def generate_units_dollar_report_excel(self):
        data = []

        if self.to_date:
            emp_units = 0.0
            comp_units = 0.0
            emp_booster_units = 0.0
            comp_booster_units = 0.0
            total_units = 0.0
            emp_value = 0.0
            comp_value = 0.0
            emp_booster_value = 0.0
            comp_booster_value = 0.0
            total_value = 0.0
            price = 0.0
            for x in self.product.product_pricing:
                if x.date_from <= self.to_date <= x.date_to:
                    price = x.price
                    print(price)
            employee = self.env["res.partner"].search(
                ['|', ('active', '=', True), ('active', '=', False), ('department', '=', 'Azimut')])
            for rec in employee:
                for record in self.env["ppf.unit"].search(
                        [('date', '<=', self.to_date), ('name', '=', rec.name), ('product', '=', self.product.id)]):
                    emp_units += record.own_units
                    comp_units += record.company_units
                    emp_booster_units += record.emp_booster_units
                    comp_booster_units += record.comp_booster_units
                    total_units += record.total_units
                emp_value = (emp_units * price) / self.currency_rate
                comp_value = (comp_units * price) / self.currency_rate
                emp_booster_value = (emp_booster_units * price) / self.currency_rate
                comp_booster_value = (comp_booster_units * price) / self.currency_rate
                total_value = (total_units * price) / self.currency_rate
                x = {rec.name: [emp_units, comp_units, emp_booster_units, comp_booster_units, total_units, emp_value,
                                comp_value, emp_booster_value, comp_booster_value, total_value]}
                data.append(x)

        self.excel_sheet_name = 'report_task.xlsx'
        # s=str(self.path)+'/'+'installment.xlsx'
        # print(s)
        self.excel_sheet_name = 'Units.xlsx'
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)

        worksheet = workbook.add_worksheet()
        # Widen the first column to make the text clearer.
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 15)
        worksheet.set_column('F:F', 15)
        worksheet.set_column('G:G', 15)
        worksheet.set_column('H:H', 15)
        worksheet.set_column('I:I', 15)
        worksheet.set_column('J:J', 15)
        worksheet.set_column('K:K', 15)

        worksheet.write('A1', 'Employee Name')
        worksheet.write('B1', 'Emp Units')
        worksheet.write('C1', 'Comp Units')
        worksheet.write('D1', 'Emp Booster Units')
        worksheet.write('E1', 'Comp Booster Units')
        worksheet.write('F1', 'Total Units')
        worksheet.write('G1', 'Emp Value in Dollar')
        worksheet.write('H1', 'Comp Value in Dollar')
        worksheet.write('I1', 'Emp Booster Value in Dollar')
        worksheet.write('J1', 'Comp Booster Value in Dollar')
        worksheet.write('K1', 'Total Value in Dollar')

        i = 1
        col = 0
        for record in data:
            worksheet.write(i, col, str(list(record.keys())[0]))
            worksheet.write_number(i, col + 1, float(list(record.values())[0][0]))
            worksheet.write_number(i, col + 2, float(list(record.values())[0][1]))
            worksheet.write_number(i, col + 3, float(list(record.values())[0][2]))
            worksheet.write_number(i, col + 4, float(list(record.values())[0][3]))
            worksheet.write_number(i, col + 5, float(list(record.values())[0][4]))
            worksheet.write_number(i, col + 6, float(list(record.values())[0][5]))
            worksheet.write_number(i, col + 7, float(list(record.values())[0][6]))
            worksheet.write_number(i, col + 8, float(list(record.values())[0][7]))
            worksheet.write_number(i, col + 9, float(list(record.values())[0][8]))
            worksheet.write_number(i, col + 10, float(list(record.values())[0][9]))

            i += 1
        workbook.close()

        output.seek(0)
        self.write({'my_file': base64.encodestring(output.getvalue())})
        return {
            "type": "ir.actions.do_nothing",
        }
