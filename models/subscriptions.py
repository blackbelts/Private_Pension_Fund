# -*- coding: utf-8 -*-

# from odoo import models, fields,  api, _
from odoo.exceptions import ValidationError
from datetime import datetime
from openerp import models, fields, api, _
from odoo import xlrd
from xlrd import open_workbook
from tempfile import TemporaryFile
import base64
from psycopg2.extensions import AsIs


class ppfSubscription(models.Model):
    _name = 'ppf.subscription'

    @api.model
    def _default_currency(self):
        return self.env.user.company_id.currency_id

    name = fields.Char(string='Subscription Batch ID', required=True, copy=False, readonly=True, index=True,
                       default=lambda self: _('New'))
    state = fields.Selection([('draft', 'Draft'), ('open', 'Invoiced'), ('paid', 'Paid')],
                             required=True, default='draft', compute='_compute_state')
    product = fields.Many2one('product.template', string='Investment Fund',
                              default=lambda self: self.env['product.template'].search([('name', '=', 'Subscription')]),
                              required=True)

    batch_date = fields.Date('Batch Date', required=True, default=datetime.today())
    invoice_ids = fields.One2many('account.invoice', 'subscription_id', string='Invoices', readonly=True)
    total_amount = fields.Float(digits=(16, 4), string='Batch Amount', compute='_compute_total_amount')
    total_own_amount = fields.Float(digits=(16, 4), string='Employee Share', compute='_compute_total_own_amount')
    total_net_to_invest = fields.Float(digits=(16, 4), string='Total Net To Invest',
                                       compute='_compute_total_net_to_invest')
    total_company_amount = fields.Float(digits=(16, 4), string='Company Share', compute='_compute_total_company_amount')
    total_emp_boosters_amount = fields.Float(digits=(16, 4), string='Emp Booster',
                                             compute='_compute_total_emp_boosters_amount')
    total_comp_boosters_amount = fields.Float(digits=(16, 4), string='Comp Booster',
                                              compute='_compute_total_comp_boosters_amount')
    total_cash = fields.Float(digits=(16, 4), string='Total Cash Pool', compute='_compute_total_cashpool', store=True)
    o_s = fields.Float('Outstanding', compute='_compute_os')
    subscription_line = fields.One2many('ppf.subscription.line', 'subscription_id', ondelete='cascade',
                                        string='Subscription Line')
    cash_pool_ids = fields.One2many('ppf.cash.pool', 'subscription_id', string="Cash Pool Lines")
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  required=True, readonly=True,
                                  default=_default_currency, track_visibility='always')
    data = fields.Binary('File')
    policy = fields.Many2one('ppf.policy', string='Investment Policy', required=True)
    department = fields.Many2one('res.partner', string='Subsidiary', domain="[('is_subcompany','=',True)]",
                                 required=True)
    invoice_created = fields.Boolean('is invoice created ?', default=False)
    number_of_subscriptions = fields.Integer('Num.Subscription', compute='_compute_total_subscriptions')
    states = fields.Selection(
        [('sub', 'Subscription'), ('open', 'OpenBalance')],
        required=True, default='sub', string='State')
    expenses = fields.Float('Expenses Reserved')

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('ppf.subscription') or 'New'
        return super(ppfSubscription, self).create(vals)

    @api.multi
    def validate(self):
        if self.subscription_line:
            for rec in self.policy.cash_type:
                self.env['ppf.cash.pool'].create({
                    'name': self.env['ir.sequence'].next_by_code('ppf.cash.pool') + '/' + str(self.name),
                    'cash_date': self.batch_date,
                    'product': rec.product.id,
                    'percentage': rec.allocation,
                    'amount': (rec.allocation * self.total_amount) / 100,
                    'subscription_id': self.id,
                })
            self.state = 'open'
        else:
            raise ValidationError(_('Please create some Subscription Lines'))
        inv = self.env['account.invoice'].create({
            # 'type': 'out_invoice',
            'partner_id': self.department.id,
            # 'account_id': self.
            'journal_id': 1,
            # 'user_id': self.env.user.id,
            'subscription_id': self.id,
            # 'origin': self.name,
            'date_due': self.batch_date,
            'invoice_line_ids': [(0, 0, {
                'name': 'Invoice For Subscription',
                'quantity': 1,
                'product_id': self.product.id,
                'price_unit': self.total_net_to_invest,
                'account_id': self.product.property_account_income_id.id,
            })],
        })
        if self.expenses:
            inv2 = self.env['account.invoice'].create({
                # 'type': 'out_invoice',
                'partner_id': self.department.id,
                # 'account_id': self.
                'journal_id': 1,
                # 'user_id': self.env.user.id,
                'subscription_id': self.id,
                # 'origin': self.name,
                'date_invoice': self.batch_date,
                'date_due': self.batch_date,
                'invoice_line_ids': [(0, 0, {
                    'name': 'Invoice For Expenses',
                    'quantity': 1,
                    'product_id': self.product.id,
                    'price_unit': self.expenses,
                    'account_id': self.env["account.account"].search([('name', '=', 'Expenses Reserved')]).id,
                })],
            })
        # inv.action_invoice_open()
        # self.state = 'paid'
        self.invoice_created = True

    # @api.multi
    # def create_invoice(self):
    #     inv = self.env['account.invoice'].create({
    #         # 'type': 'out_invoice',
    #         'partner_id': self.department.id,
    #         # 'account_id': self.
    #         # 'user_id': self.env.user.id,
    #         'subscription_id': self.id,
    #         # 'origin': self.name,
    #         'date_due': self.batch_date,
    #         'invoice_line_ids': [(0, 0, {
    #             'name': 'Invoice For Subscription',
    #             'quantity': 1,
    #             'price_unit': self.total_amount,
    #             'account_id': self.department.account_payable.id,
    #         })],
    #     })
    #     # inv.action_invoice_open()
    #     # self.state = 'paid'
    #     self.invoice_created = True

    @api.one
    @api.depends('invoice_ids')
    def _compute_state(self):
        self.state = 'draft'
        if self.invoice_created == True:
            self.state = 'open'
        for record in self.invoice_ids:
            if record.subscription_id.id == self.id:
                if record.state == 'paid':
                    self.state = 'paid'
            else:
                return ('false')

    @api.one
    @api.depends('subscription_line')
    def _compute_total_amount(self):
        # self.total_amount = 0.0
        for record in self.subscription_line:
            self.total_amount += record.total

    @api.one
    @api.depends('subscription_line')
    def _compute_total_own_amount(self):
        self.total_own_amount = 0.0
        for record in self.subscription_line:
            self.total_own_amount += record.own

    @api.one
    @api.depends('subscription_line')
    def _compute_total_net_to_invest(self):
        self.total_net_to_invest = 0.0
        for record in self.subscription_line:
            self.total_net_to_invest += record.net_to_invest

    @api.one
    @api.depends('subscription_line')
    def _compute_total_company_amount(self):
        self.total_company_amount = 0.0
        for record in self.subscription_line:
            self.total_company_amount += record.company

    @api.one
    @api.depends('subscription_line')
    def _compute_total_emp_boosters_amount(self):
        self.total_emp_boosters_amount = 0.0
        for record in self.subscription_line:
            self.total_emp_boosters_amount += record.emp_booster

    @api.one
    @api.depends('subscription_line')
    def _compute_total_comp_boosters_amount(self):
        self.total_boosters_comp_amount = 0.0
        for record in self.subscription_line:
            self.total_comp_boosters_amount += record.comp_booster

    @api.one
    @api.depends('cash_pool_ids')
    def _compute_total_cashpool(self):
        self.total_cash = 0.0
        for record in self.cash_pool_ids:
            self.total_cash += record.amount

    @api.one
    @api.depends('subscription_line')
    def _compute_total_subscriptions(self):
        self.number_of_subscriptions = len(self.subscription_line)

    @api.one
    @api.depends('total_cash')
    def _compute_os(self):
        self.o_s = self.total_amount - self.total_cash

    @api.model_cr
    @api.multi
    def import_file(self):
        wb = open_workbook(file_contents=base64.decodestring(self.data))
        sheet = wb.sheets()[0]

        for s in wb.sheets():

            values = []

            for row in range(1, s.nrows):

                col_value = []

                for col in range(s.ncols):
                    value = (s.cell(row, col).value)

                    # try:
                    #     value = str(int(value))
                    #     value=float(value)
                    # except:
                    #     pass

                    col_value.append(value)
                print('20202020202020')
                print(col_value)
                # name = self.env['res.partner'].search([('name', '=',col_value[0])]).id
                # total = (col_value[1] *(col_value[2]/100))+col_value[3]+col_value[4]+col_value[5]
                self.env.cr.execute("INSERT INTO  ppf_subscription_line (member_name, member_id,states,"
                                    " own, company,"
                                    "emp_booster,comp_booster, total, subscription_id) VALUES (%s,%s,%s,%s,%s,%s,%s,"
                                    "%s,%s) "
                                    , (self.env['res.partner'].search([('name', '=', col_value[0])]).id, col_value[1],
                                       col_value[2],
                                       col_value[3], col_value[4],
                                       col_value[5], col_value[6],
                                       col_value[3] + col_value[4] + col_value[5] + col_value[6], self.id))
                # self.update({
                # 'subscription_line': [(0, 0, {'member_name':(self.env['res.partner'].search([('name', '=',col_value[0])]).id), 'salary': col_value[1],'perc_salary': col_value[2],
                #                               'own':col_value[3],'company':col_value[4],'booster':col_value[5]})],
                # })

            values.append(col_value)
        print(values)

    @api.multi
    def sub_print(self):
        return self.env.ref('Private_Pension_Fund.subs').report_action(self)

    @api.multi
    def send_mail_sub(self):
        # Find the e-mail template
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        template_id = self.env.ref('Private_Pension_Fund.sub_email_template')
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'ppf.subscription',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            # 'custom_layout': "sale.mail_template_data_notification_email_sale_order",
            'proforma': self.env.context.get('proforma', False),
            'force_email': True
        }

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class subscriptionLine(models.Model):
    _name = 'ppf.subscription.line'

    @api.multi
    @api.onchange('member_name')
    def get_account(self):
        account = self.env['account.account'].search([('id', '=', '1')])
        self.account_id = account.id
        self.name = "aya 7aga"

    member_name = fields.Many2one('res.partner', string='Employee Name', domain="[('company_type','=','person')]",
                                  required=True)
    department = fields.Many2one('res.partner', related='member_name.department')
    member_id = fields.Char(string='Employee ID', readonly=True)
    total_amount = fields.Float(digits=(16, 4), string='Batch Amount', related='subscription_id.total_amount')
    loan_id = fields.Many2one('ppf.loan', string='Loan')
    surrender_id = fields.Many2one('ppf.surrender', string='Surrender')
    salary = fields.Float(string='Salary')
    perc = fields.Float(digits=(16, 4), string='Percentage', compute='_compute_percentage')
    own = fields.Float(digits=(16, 4), string='Own')
    # total_own = fields.Float('Total Own', compute='_compute_total_own' )
    company = fields.Float(digits=(16, 4), string='Company')
    emp_booster = fields.Float(digits=(16, 4), string='Emp Booster')
    comp_booster = fields.Float(digits=(16, 4), string='Comp Booster')
    total = fields.Float(digits=(16, 4), string='Total Amount', store=True, readonly=True, compute='_compute_total')
    subscription_id = fields.Many2one('ppf.subscription', ondelete='cascade')
    states = fields.Selection(
        [('sub', 'Subscription'), ('loan', 'Loan'), ('surrender', 'Surrender'), ('open', 'OpenBalance')],
        required=True, string='State')
    expenses = fields.Float(digits=(16, 4), string='Expenses', compute='_compute_expenses')
    net_to_invest = fields.Float(digits=(16, 4), string='Net To Invest', compute='_compute_net_to_invest')

    @api.one
    @api.depends('own', 'company', 'emp_booster','comp_booster')
    def _compute_total(self):
        self.total = self.own + self.company + self.emp_booster + self.comp_booster

    # @api.one
    # @api.depends('subscription_id')
    # def _compute_state(self):
    #     self.states = self.subscription_id.states

    @api.one
    @api.depends('total_amount', 'total')
    def _compute_percentage(self):
        if self.subscription_id.total_amount:
            self.perc = (self.total / self.subscription_id.total_amount) * 100

    @api.one
    @api.depends('subscription_id', 'perc')
    def _compute_expenses(self):
        self.expenses = (self.perc * self.subscription_id.expenses) / 100

    @api.one
    @api.depends('total', 'expenses')
    def _compute_net_to_invest(self):
        self.net_to_invest = self.total - self.expenses


class AccountInvoiceRelate(models.Model):
    _inherit = 'account.invoice'

    subscription_id = fields.Many2one('ppf.subscription', string='Subscription')
