from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime
from dateutil.relativedelta import relativedelta


class product_fund(models.Model):
    _inherit = 'product.template'

    product_id = fields.Char('Product id')
    Assest_manger = fields.Many2one('res.partner', 'Asset Manger')
    issuer = fields.Many2one('res.partner', 'Issuer')
    product_unit = fields.Char('Product Unit')
    face_value = fields.Many2one('res.currency', 'Face Value')
    type = fields.Selection([
        ('consu', 'Consumable'),
        ('service', 'Service'), ('financial', 'Financial')], string='Product Type', default='financial', required=True)
    # type2 = fields.Selection([
    #     ('financial', 'Financial')], string='Product Type', readonly=True)
    product_compos = fields.One2many('product.composition', 'product')
    product_pricing = fields.One2many('product.pricing', 'product_price')
    product_reports = fields.One2many('product.reports', 'product_reports')
    dtd = fields.Float('Day Performance')
    wtd = fields.Float('Week Performance')
    mtd = fields.Float('Month Performance')
    qtd = fields.Float('Quarter Performance')
    ytd = fields.Float('Year Performance')
    ytd2 = fields.Float('2 Years Performance')
    ytd3 = fields.Float('3 Years Performance')
    ytd4 = fields.Float('4 Years Performance')

    # income_account = fields.Many2one('account.account', 'Income Account', required=True,
    #                                  default=lambda self: self.env['account.account'].search(
    #                                      [('name', '=', 'Income Account')]))
    # expense_account = fields.Many2one('account.account', 'Expense Account', required=True,
    #                                   default=lambda self: self.env['account.account'].search(
    #                                       [('name', '=', 'Expense Account')]))

    # @api.multi
    # def get_performance(self):
    #     prices = {}
    #     for rec in self.env['product.pricing'].search([('product_price', '=', self.id)], order="id desc"):
    #         prices[rec.id] = rec.price
    #     print(prices)
    #     for k, v in prices.items():
    #         rec = self.env['product.pricing'].search([('id', '=', k)])
    #         rec.write({'performance': rec.price - prices[k - 1]})
    # @api.multi
    @api.model
    def get_quarter_performance(self, year):
        data = {}
        products = self.env['product.template'].search([('type', '=', 'financial')])
        print(products)
        for x in products:
            prices = {}
            # year = 2019
            # ids=self.env['product.pricing'].search([('product_price','=',self.id)]).ids
            records = self.env['product.pricing'].search([('date_to', 'like', year), ('product_price', '=', x.id)])
            print(records)
            rec = self.env['product.pricing'].search([('date_to', 'like', year), ('product_price', '=', x.id)],
                                                     order='id desc', limit=1)
            print(len(records.ids), x)
            while rec:
                print(111111111)
                if rec.date_to:
                    date = datetime.strptime(rec.date_to, '%Y-%m-%d').date()
                    last_month = date - relativedelta(months=3)
                    if last_month.month == 3:
                        last_month = last_month + relativedelta(days=1)
                    last_date = str(last_month)
                    print(date)
                    print(last_month)
                    prev_rec = self.env['product.pricing'].search(
                        [('date_to', 'like', year), ('date_to', '=', last_date), ('product_price', '=', x.id)])
                    if prev_rec:
                        performance = (rec.price / prev_rec.price) - 1
                    else:
                        performance = 1
                    print(rec.price, prev_rec.price, performance)
                    rec = prev_rec
                    prices[str(date)] = performance
            data[str(x.name)] = prices
        print(data)
        return data

    # @api.multi
    @api.model
    def get_yearly_performance(self):
        data = {}
        products = self.env['product.template'].search([('type', '=', 'financial')])
        print(products)
        for x in products:
            prices = {}
            # year = 2019
            # ids=self.env['product.pricing'].search([('product_price','=',self.id)]).ids
            records = self.env['product.pricing'].search([('product_price', '=', x.id)])
            print(records)
            rec = self.env['product.pricing'].search([('product_price', '=', x.id)],
                                                     order='id desc', limit=1)
            print(len(records.ids), x)
            while rec:
                print(111111111)
                if rec.date_to:
                    date = datetime.strptime(rec.date_to, '%Y-%m-%d').date()
                    last_year = date - relativedelta(years=1)

                    prev_rec = self.env['product.pricing'].search(
                        [('date_to', '=', last_year), ('product_price', '=', x.id)])
                    if prev_rec:
                        performance = (rec.price / prev_rec.price) - 1
                    else:
                        performance = 1
                    print(rec.price, prev_rec.price, performance)
                    rec = prev_rec
                    prices[str(date)] = performance
            data[str(x.name)] = prices
        print(data)
        return data


class Product_Composition(models.Model):
    _name = 'product.composition'
    item = fields.Char('Item')
    perc = fields.Integer('Percentage')
    cat = fields.Many2one('product.category')
    product = fields.Many2one('product.template')





class Product_Price(models.Model):
    _name = 'product.pricing'
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    price = fields.Float(digits=(16, 4), string='Price')
    product_price = fields.Many2one('product.template', ondelete='cascade')
    performance = fields.Float(string='Performance')

    # @api.multi
    @api.onchange('price')
    def get_performance(self):
        prices = {}
        if self.price:
            for rec in self.product_price.product_pricing.search([], order='id desc', limit=1):
                self.performance = (self.price / rec.price) - 1
                print(rec.id)
                print(self.price, rec.price)
                print(self.performance)
                print(22222222222)
        # for k,v in prices.items():


class Product_Reports(models.Model):
    _name = 'product.reports'
    name = fields.Char('Report')
    title = fields.Char('Report Title')
    product_reports = fields.Many2one('product.template')


class Partners(models.Model):
    _inherit = 'res.partner'

    birth_date = fields.Date('Date of Birth')
    hiring_date = fields.Date('Hiring Date')
    subscription_date = fields.Date('Subscription Start Date', default=datetime.today())
    pension_date = fields.Date('Pension Date')
    member_id = fields.Char('Employee ID')
    job_title = fields.Char('Job Title')
    grade = fields.Char('Grade')
    benef = fields.Char('Beneficiary')

    martiual_status = fields.Selection([('Single', 'Single'),
                                        ('Married', 'Married'), ],
                                       'Marital Status', track_visibility='onchange')

    sub_count = fields.Integer(compute='_compute_sub_count')
    department = fields.Many2one('res.partner', string='Subsidiary', domain="[('is_subcompany','=',True)]")
    sub_department = fields.Many2one('ppf.department', string='Department')
    total_value = fields.Float('Total Value')
    ratio = fields.Float('percentage')
    company_share = fields.Float('Company Share')
    employee_share = fields.Float('Employee Share')
    boosters = fields.Float('Boosters')
    member_count = fields.Integer(compute='_compute_member_count')

    @api.one
    def _compute_member_count(self):
        if self.is_subcompany == 1:
            for partner in self:
                sub_lines = self.env['res.partner'].search([('department', '=', self.id)]).ids

                partner.member_count = self.env['res.partner'].search_count(
                    [('department', '=', self.id)])

    @api.one
    def _compute_sub_count(self):
        if self.customer == 1:
            for partner in self:
                sub_lines = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)]).ids

                partner.sub_count = self.env['ppf.subscription'].search_count(
                    [('subscription_line', 'in', sub_lines)])
        if self.is_subcompany == 1:
            for partner in self:
                sub_lines = self.env['ppf.subscription'].search([('department', '=', self.id)]).ids

                partner.sub_count = self.env['ppf.subscription'].search_count(
                    [('department', '=', self.id)])

    @api.multi
    def show_partner_members(self):
        tree_view = self.env.ref('Private_Pension_Fund.res_partner_view_remove1')
        form_view = self.env.ref('Private_Pension_Fund.view_partner_form_inherit_customer')
        if self.is_subcompany == 1:
            return {
                'name': ('Members'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'ppf.subscription',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_is_subcompany': self.id},
                'domain': [('department', '=', self.id)]
            }

    @api.multi
    def show_partner_subs(self):
        tree_view = self.env.ref('Private_Pension_Fund.subscription_tree')
        form_view = self.env.ref('Private_Pension_Fund.form_subscription')
        if self.customer == 1:
            return {
                'name': ('Subs'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'ppf.subscription',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_customer': self.id},
                'domain': [('subscription_line.member_name', '=', self.id)]
            }
        if self.is_subcompany == 1:
            return {
                'name': ('Subs'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'ppf.subscription',  # model name ?yes true ok
                'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_is_subcompany': self.id},
                'domain': [('department', '=', self.id)]
            }

    @api.multi
    def search_sub(self):
        sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
        inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
        return inv

    @api.multi
    def compute_total_sub(self):
        sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
        print(sub)
        sum = 0.0
        for rec in sub:
            print(555555555555)
            sum += rec.total
        # inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
        print(sum)
        return sum

    # @api.multi
    # def search_sub(self):
    #     sub=self.env['account.invoice.line'].search([('member_name', '=', self.id)])
    #     inv=self.env['account.invoice'].search([('invoice_line_ids','in',sub.ids)])
    #     return inv
    #
    #
    #

    # @api.multi
    # def search_cash_pool(self):
    #     sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
    #     inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
    #     all_lines = self.env['allocation.lines'].search([('sub', 'in', inv.ids)])
    #     allocation = self.env['allocation'].search([('allocation_line', 'in', all_lines.ids)])
    #     # investment = self.env['account.invoice'].search([('allocation_id', 'in', allocation.ids)])
    #
    #     return allocation

    #
    #
    # @api.multi
    # def search_invest(self):
    #     sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
    #     inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
    #     invest1=[]
    #     for data in inv:
    #           allocation = self.env['cash.pool'].search([('subscription_id', 'in', data.ids)])
    #           investment = self.env['account.invoice'].search([('allocation_id', 'in', allocation.ids)])
    #           # invest1.append(investment)
    #           return investment

    @api.multi
    def search_invest(self):
        sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
        inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
        print(inv)
        # invest1 = self.env['account.invoice']
        invest2 = []
        for data in inv:
            allocation = self.env['ppf.cash.pool'].search([('subscription_id', 'in', data.ids)])
            for cash in allocation:
                investment = self.env['ppf.investment'].search([('cash_pool_id', 'in', cash.ids)])
                for i in investment:
                    invest2.append(i.id)

        invest = self.env['ppf.investment'].search([('id', 'in', invest2)])
        print(invest)
        return invest

    @api.multi
    def compute_units(self):
        sub = self.env['ppf.subscription.line'].search([('member_name', '=', self.id)])
        inv = self.env['ppf.subscription'].search([('subscription_line', 'in', sub.ids)])
        # invest1 = self.env['account.invoice']
        invest2 = []
        for data in inv:
            allocation = self.env['ppf.cash.pool'].search([('subscription_id', 'in', data.ids)])
            for cash in allocation:
                investment = self.env['ppf.investment'].search([('cash_pool_id', 'in', cash.ids)])
                for i in investment:
                    invest2.append(i.id)

        invest = self.env['ppf.investment'].search([('id', 'in', invest2)])
        for rec2 in invest:
            units = 0
            for rec in rec2.investment_line_ids:
                print(55555555555555555555)
                units += rec.quantity

        print(units)
        return units

    @api.model
    def _default_fund(self):
        return self.env['ppf.fund'].search([], limit=1)

    is_subcompany = fields.Boolean('Subsidiary')
    department_number = fields.Integer('Subsidiary Code')
    employees = fields.Integer('Employees', compute='_compute_total_employee')
    total_value_sub = fields.Float('Total Amount', compute='_compute_total_amount')
    percentage = fields.Float('Percentage')
    company_share_sub = fields.Float('Company Share', compute='_compute_total_company_amount')
    employee_share_sub = fields.Float('Employee Share', compute='_compute_total_own_amount')
    emp_boosters_sub = fields.Float('Emp Boosters', compute='_compute_total_boosters_amount')
    comp_boosters_sub = fields.Float('Comp Boosters', compute='_compute_total_boosters_amount')

    fund = fields.Many2one('ppf.fund', string='Fund', default=_default_fund)
    subscription_ids = fields.One2many('ppf.subscription', 'department')
    units_ids = fields.One2many('ppf.unit', 'department2')
    own_units = fields.Float('Employee Units', compute='_compute_total_own_units')
    company_units = fields.Float('Company Units', compute='_compute_total_company_units')
    emp_booster_units = fields.Float('Emp Booster Units', compute='_compute_total_boosters_units')
    comp_booster_units = fields.Float('Comp Booster Units', compute='_compute_total_boosters_units')
    total_units = fields.Float('Total Units', compute='_compute_total_units')

    # account_payable = fields.Many2one('account.account', string='Payable Account',
    #                                   domain="[('user_type_id','=','Payable')]")
    # account_receivable = fields.Many2one('account.account', string='Receivable Account',
    #                                      domain="[('user_type_id','=','Receivable')]")

    _sql_constraints = [
        ('department_number_uniq',
         'UNIQUE (department_number)',
         'Department Number must be unique.')
    ]

    @api.one
    def _compute_total_amount(self):
        unit = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in unit:
            self.total_value_sub += record.total_value
            print(self.total_value)

    @api.one
    def _compute_total_own_amount(self):
        unit = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in unit:
            self.employee_share_sub += record.own

    @api.one
    def _compute_total_company_amount(self):
        unit = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in unit:
            self.company_share_sub += record.company

    @api.one
    def _compute_total_boosters_amount(self):
        unit = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in unit:
            self.emp_boosters_sub += record.emp_booster
            self.comp_boosters_sub += record.comp_booster

    @api.one
    @api.model
    def _compute_total_employee(self):
        employees = len(self.env['res.partner'].search([('department', '=', self.id)]))
        self.employees = employees

    @api.one
    def _compute_total_units(self):
        units = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in units:
            self.total_units += record.total_units

    @api.one
    def _compute_total_own_units(self):
        units = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in units:
            self.own_units += record.own_units

    @api.one
    def _compute_total_company_units(self):
        units = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in units:
            self.company_units += record.company_units

    @api.one
    def _compute_total_boosters_units(self):
        units = self.env['ppf.unit'].search([('department', '=', self.id)])
        for record in units:
            self.emp_booster_units += record.emp_booster_units
            self.comp_booster_units += record.comp_booster_units


class LoanAndSurrenderRules(models.Model):
    _name = 'loan.rules'

    rule_id = fields.Integer('Rule ID', default=1, readonly=True)
    first_loan_rule = fields.Integer('Loan Allowed After(Period in Months):')
    second_loan_rule = fields.Integer('Loan Allowed Every(Period in Months):')
    third_loan_rule = fields.Float('Loan Rule for Company Units in First 5 Years:')
    fourth_loan_rule = fields.Float('Loan Rule for Company Units After First 5 Years:')
    first_surr_rule = fields.Float('Surrender Rule for Year 1 :')
    second_surr_rule = fields.Float('Surrender Rule for Year 2 :')
    third_surr_rule = fields.Float('Surrender Rule for Year 3 :')
    fourth_surr_rule = fields.Float('Surrender Rule for Year 4 :')
    fifth_surr_rule = fields.Float('Surrender Rule for Year 5 :')
    final_surr_rule = fields.Float('Surrender Rule After 5 Years :')
