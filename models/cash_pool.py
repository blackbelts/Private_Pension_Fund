from odoo import models, fields, api


class cashPool(models.Model):
    _name = 'ppf.cash.pool'
    _rec_name = 'name'

    @api.model
    def _default_currency(self):
        return self.env.user.company_id.currency_id

    name = fields.Char('ID', required=True)
    state = fields.Selection([('o/s', 'O/S'), ('inv', 'Invested')],
                             required=True, default='o/s')
    cash_date = fields.Date('Date')
    percentage = fields.Float('Percentage', default=100, required=True)
    product = fields.Many2one('product.template', string='Investment Fund')
    type = fields.Many2one('product.category', string='Type')
    amount = fields.Float('Amount', compute='_compute_amount', store=True, digits=(16, 4))
    perv_amount = fields.Float('Previous Invested', )
    os_amount = fields.Float('Outstanding', compute='_compute_os_amount', digits=(16, 4))
    subscription_id = fields.Many2one('ppf.subscription', string='Sub')
    allocation_line_invest = fields.One2many('ppf.investment', 'cash_pool_id')

    currency_id = fields.Many2one('res.currency', string='Currency',
                                  required=True, readonly=True, default=_default_currency, track_visibility='always')

    @api.one
    @api.depends('percentage')
    def _compute_amount(self):
        self.amount = (self.percentage * (self.subscription_id.total_net_to_invest)) / 100

    # @api.one
    # @api.depends('allocation_line_invest')
    # def _compute_perv_amount(self):
    #     self.perv_amount = 0.0
    #     for record in self.allocation_line_invest:
    #         self.perv_amount += record.total_amount

    @api.one
    @api.depends('perv_amount')
    def _compute_os_amount(self):
        self.os_amount = self.amount

    # @api.one
    # @api.depends('allocation_line_invest')
    # def _compute_state(self):
    #     self.state = 'o/s'
    #     print(111111111)
    #     for rec in self.allocation_line_invest:
    #         if rec.cash_pool_id.id == self.id:
    #             if rec.state == 'paid':
    #                 self.state = 'inv'
